#!/usr/bin/python

from Cheetah.Template import Template

def generate_gitlab_ci_yml():
    gitlab_ci_yml_tmpl = open(".gitlab-ci.yml.tmpl", "r")
    template_content = gitlab_ci_yml_tmpl.read()
    template = Template(template_content)
    template.imageJobMap = [("registry.gitlab.com/document-scanner/jhbuild-java-wrapper:maven-build-latest", "maven358jdk8"),
            ("registry.gitlab.com/document-scanner/jhbuild-java-wrapper:ubuntu-build-latest", "ubuntu1910")]
    output = str(template)
    gitlab_ci_yml = open(".gitlab-ci.yml", "w")
    gitlab_ci_yml.write(output)
    gitlab_ci_yml.flush()
    gitlab_ci_yml.close()
    gitlab_ci_yml_tmpl.close()

if __name__ == "__main__":
    generate_gitlab_ci_yml()
