#!/bin/sh -e

docker login registry.gitlab.com
docker build --pull -t registry.gitlab.com/document-scanner/jhbuild-java-wrapper:maven-build-latest -f docker/Dockerfile-maven .
docker build --pull -t registry.gitlab.com/document-scanner/jhbuild-java-wrapper:ubuntu-build-latest -f docker/Dockerfile-ubuntu .
docker push registry.gitlab.com/document-scanner/jhbuild-java-wrapper:maven-build-latest
docker push registry.gitlab.com/document-scanner/jhbuild-java-wrapper:ubuntu-build-latest
