/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper.download;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 */
public class DownloadUtilsTest {

    @Test(expected = NullPointerException.class)
    public void parseUbuntuVersionNull() {
        DownloadUtils.parseUbuntuVersion(null);
    }

    @Test
    public void parseUbuntuVersionEmpty() {
        assertNull(DownloadUtils.parseUbuntuVersion(""));
    }

    @Test
    public void parseUbuntuVersionNoVersion() {
        assertNull(DownloadUtils.parseUbuntuVersion("123.abcdefg456"));
    }

    @Test
    public void parseUbuntuVersionNoHotfix() {
        UbuntuVersion result = DownloadUtils.parseUbuntuVersion("Ubuntu 14.04");
        assertEquals(new UbuntuVersion(14,4),
                result);
    }

    @Test
    public void parseUbuntuVersionHotfix() {
        UbuntuVersion result = DownloadUtils.parseUbuntuVersion("Ubuntu 14.04.5");
        assertEquals(new UbuntuVersion(14, 4, 5),
                result);
    }

    @Test
    public void parseUbuntuVersionLTS() {
        UbuntuVersion result = DownloadUtils.parseUbuntuVersion("Ubuntu 14.04 LTS");
        assertEquals(new UbuntuVersion(14, 4),
                result);
    }

    @Test
    public void parseUbuntuVersionTrailingFileContent() {
        UbuntuVersion result = DownloadUtils.parseUbuntuVersion("Ubuntu 14.04.5\t\nxyz");
        assertEquals(new UbuntuVersion(14, 4, 5),
                result);
    }

    @Test(expected = NullPointerException.class)
    public void parseDebianVersionNull() {
        DownloadUtils.parseDebianVersion(null);
    }

    @Test
    public void parseDebianVersionEmpty() {
        assertNull(DownloadUtils.parseDebianVersion(""));
    }

    @Test
    public void parseDebianVersionNoVersion() {
        assertNull(DownloadUtils.parseDebianVersion("123.abcdefg456"));
    }

    @Test
    public void parseDebianVersion() {
        DebianVersion result = DownloadUtils.parseDebianVersion("Debian GNU/Linux 10");
        assertEquals(new DebianVersion(10),
                result);
    }

    @Test
    public void parseDebianVersionTrailingFileContent() {
        DebianVersion result = DownloadUtils.parseDebianVersion("Debian GNU/Linux 10\t\nxyz");
        assertEquals(new DebianVersion(10),
                result);
    }
}