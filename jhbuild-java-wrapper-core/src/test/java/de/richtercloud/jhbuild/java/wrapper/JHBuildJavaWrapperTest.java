/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper;

import com.google.common.collect.Sets;
import de.richtercloud.execution.tools.*;
import de.richtercloud.jhbuild.java.wrapper.download.AutoDownloader;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadException;
import de.richtercloud.jhbuild.java.wrapper.download.ExtractionException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.*;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.*;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
@RunWith(PowerMockRunner.class)
public class JHBuildJavaWrapperTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(JHBuildJavaWrapperTest.class);
    private final static Random RANDOM;
    static {
        long randomSeed = System.currentTimeMillis();
        LOGGER.debug(String.format("using random seed %d",
                randomSeed));
        RANDOM = new Random(randomSeed);
    }
    private static final String TEST_PREFIX = "jhbuild-java-wrapper-test-prefix";
    private static final String DOWNLOAD_PREFIX = "jhbuild-java-wrapper-test-download";
    private static final String VALIDATE_BINARY = "validateBinary";
    /**
     * Reflect changes to the test method name automatically. There seems to be no easy way to print the test method
     * name before start although the realization would be rather trivial. Needs to be {@code public}.
     */
    @Rule
    public TestName name = new TestName();

    @Test
    public void testCalculateParallelism() {
        LOGGER.info(name.getMethodName());
        int result = JHBuildJavaWrapper.calculateParallelism();
        assertTrue(result >= 1);
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void testInitInvalidParallelismZero() throws IOException {
        LOGGER.info(name.getMethodName());
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        //test IllegalArgumentException for invalid parallelism values
        new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                0);
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void testInitInvalidParallelismNegative() throws IOException {
        LOGGER.info(name.getMethodName());
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        int parallelism = RANDOM.nextInt();
        while(parallelism >= 1) {
            parallelism = RANDOM.nextInt();
        }
        LOGGER.debug(String.format("testing parallelism %d",
                parallelism));
        new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                parallelism);
    }

    /**
     * Tests failure on missing CC binary. Needs to be a separate method in
     * order to allow static mocking with PowerMockito which cannot be reset
     * (see
     * https://stackoverflow.com/questions/48831432/how-to-undo-reset-powermockito-mockstatic
     * for eventual solutions).
     *
     * @throws IOException if such an exception occurs
     * @throws Exception if such an exception occurs
     */
    @Test(expected = MissingSystemBinaryException.class)
    @PrepareForTest(BinaryUtils.class)
    public void testInstallModulesetStringInexistingCC() throws IOException,
            Exception {
        LOGGER.info(name.getMethodName());
        JHBuildJavaWrapper instance = generateDefaultTestInstance();
        mockStatic(BinaryUtils.class);
        doThrow(new BinaryValidationException("simulating gcc not present")).when(BinaryUtils.class,
                VALIDATE_BINARY,
                eq("gcc"),
                anyString(),
                anyString());
        String moduleName = "postgresql-9.6.3";
        instance.installModuleset(moduleName);
    }

    @Test(expected = MissingSystemBinaryException.class)
    @PrepareForTest(BinaryUtils.class)
    public void testInstallModulesetStringInexistingMake() throws Exception {
        LOGGER.info(name.getMethodName());
        JHBuildJavaWrapper instance = generateDefaultTestInstance();
        mockStatic(BinaryUtils.class);
        doThrow(new BinaryValidationException("simulating make not present")).when(BinaryUtils.class,
                VALIDATE_BINARY,
                eq("make"),
                anyString(),
                anyString());
        String moduleName = "postgresql-9.6.3";
        instance.installModuleset(moduleName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstallModulesetInputStreamStringNull() throws IOException,
            ExtractionException,
            InterruptedException,
            MissingSystemBinaryException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException {
        LOGGER.info(name.getMethodName());
        String moduleName = null;
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        JHBuildJavaWrapper instance = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
        instance.installModuleset(null,
                moduleName);
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressWarnings("PMD.CloseResource")
    public void testInstallModulesetInputStreamStringModuleNameNull() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        LOGGER.info(name.getMethodName());
        String moduleName = null;
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        InputStream modulesetInputStream = mock(InputStream.class);
        JHBuildJavaWrapper instance = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
        instance.installModuleset(modulesetInputStream,
                moduleName);
    }

    @Test(expected = IllegalArgumentException.class)
    @SuppressWarnings("PMD.CloseResource")
    public void testInstallModulesetInputStreamStringModuleNameEmpty() throws InterruptedException,
            BuildFailureException,
            IOException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        LOGGER.info(name.getMethodName());
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        String moduleName = "";
        InputStream modulesetInputStream = mock(InputStream.class);
        JHBuildJavaWrapper instance = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
        instance.installModuleset(modulesetInputStream,
                moduleName);
    }

    @Test(expected = ModuleBuildFailureException.class)
    @PrepareForTest({BinaryUtils.class, ExecutionUtils.class})
    public void testInstallModulesetInexistingModuleName() throws Exception {
        LOGGER.info(name.getMethodName());
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        String moduleName = "inexisting";
        final String jhbuildFakeBinary = "jhbuildExistingFake";
        mockStatic(BinaryUtils.class);
        doNothing().when(BinaryUtils.class,
                VALIDATE_BINARY,
                eq(jhbuildFakeBinary),
                anyString(),
                anyString());
        mockStatic(ExecutionUtils.class);
        Process processMock = mock(Process.class);
        doReturn(1).when(processMock).exitValue();
        Triple<Process, OutputReaderThread, OutputReaderThread> processCreationResultMock = new ImmutableTriple<>(processMock,
                new OutputReaderThread(new ByteArrayInputStream(new byte[0])),
                new OutputReaderThread(new ByteArrayInputStream(new byte[0])));
        doReturn(processCreationResultMock).when(ExecutionUtils.class,
                "createProcess",
                isNull(),
                any(Map.class),
                anyString(),
                any(List.class),
                any(List.class),
                eq(OutputReaderThreadMode.OUTPUT_STREAM),
                eq(jhbuildFakeBinary),
                anyString(),
                eq("--exit-on-error"),
                eq("bootstrap"));
        JHBuildJavaWrapper instance = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                jhbuildFakeBinary,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304 / 2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
        try (InputStream modulesetInputStream = JHBuildJavaWrapper.class.getResourceAsStream("/moduleset-default.xml")) {
            instance.installModuleset(modulesetInputStream,
                    moduleName);
        }
    }

    @Test(expected = JHBuildJavaWrapper.LibraryNotPresentException.class)
    public void testCheckLibPresenceInexisting() throws IOException,
            JHBuildJavaWrapper.LibraryNotPresentException {
        LOGGER.info(name.getMethodName());
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        JHBuildJavaWrapper instance = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
        String inexistingPc = "inexisting.pc";
        instance.checkLibPresence(installationPrefixDir,
                inexistingPc);
    }

    @Test
    public void testCheckLibPresenceExisting() throws IOException,
            JHBuildJavaWrapper.LibraryNotPresentException {
        LOGGER.info(name.getMethodName());
        File installationPrefixDir = createFakeInstallationPrefixDir();
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
        ).toFile();
        String existingBinaryFake = createExistingBinaryFake();
        JHBuildJavaWrapper instance = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                existingBinaryFake,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
        String existingPcName = "existing.pc";
        Path existingPc = Paths.get(installationPrefixDir.getAbsolutePath(), existingPcName);
        try {
            Files.write(existingPc,
                    "".getBytes());
            Path result = instance.checkLibPresence(installationPrefixDir,
                    existingPcName);
            assertEquals(existingPc,
                    result);
        }finally {
            existingPc.toFile().delete();
        }
    }

    private JHBuildJavaWrapper generateDefaultTestInstance() throws IOException {
        File installationPrefixDir = createFakeInstallationPrefixDir();
        LOGGER.debug(String.format("installationPrefixDir: %s",
                installationPrefixDir.getAbsolutePath()));
        File downloadDir = Files.createTempDirectory(DOWNLOAD_PREFIX //prefix
                ).toFile();
        LOGGER.debug(String.format("downloadDir: %s",
                downloadDir.getAbsolutePath()));
        return generateDefaultTestInstance(installationPrefixDir,
                downloadDir);
    }

    private JHBuildJavaWrapper generateDefaultTestInstance(File installationPrefixDir,
            File downloadDir) throws IOException {
        return new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                JHBuildJavaWrapper.GIT_DEFAULT,
                JHBuildJavaWrapper.JHBUILD_DEFAULT,
                JHBuildJavaWrapper.SH_DEFAULT,
                JHBuildJavaWrapper.MAKE_DEFAULT,
                JHBuildJavaWrapper.PYTHON3_DEFAULT,
                JHBuildJavaWrapper.CC_DEFAULT,
                JHBuildJavaWrapper.MSGFMT_DEFAULT,
                JHBuildJavaWrapper.CPAN_DEFAULT,
                JHBuildJavaWrapper.PATCH_DEFAULT,
                JHBuildJavaWrapper.OPENSSL_DEFAULT,
                JHBuildJavaWrapper.WGET_DEFAULT,
                JHBuildJavaWrapper.PKG_CONFIG_DEFAULT,
                JHBuildJavaWrapper.XZ_DEFAULT,
                JHBuildJavaWrapper.M4_DEFAULT,
                new AutoDownloader(),
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/2,
                true,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                JHBuildJavaWrapper.calculateParallelism());
    }

    private String createExistingBinaryFake() throws IOException {
        return Files.createTempFile(JHBuildJavaWrapperTest.class.getSimpleName(), "fakeBinary",
                PosixFilePermissions.asFileAttribute(Sets.newHashSet(PosixFilePermission.OWNER_EXECUTE))).toString();
    }

    private File createFakeInstallationPrefixDir() throws IOException {
        Path installationPrefixDir = Files.createTempDirectory(TEST_PREFIX);
        Files.createFile(Paths.get(installationPrefixDir.toString(), JHBuildJavaWrapper.ZLIB_PC));
        Files.createFile(Paths.get(installationPrefixDir.toString(), JHBuildJavaWrapper.OPENSSL_PC));
        Files.createFile(Paths.get(installationPrefixDir.toString(), JHBuildJavaWrapper.GMP_PC));
        Files.createFile(Paths.get(installationPrefixDir.toString(), JHBuildJavaWrapper.NETTLE_PC));
        Files.createFile(Paths.get(installationPrefixDir.toString(), JHBuildJavaWrapper.LZMA_PC));
        Files.createFile(Paths.get(installationPrefixDir.toString(), JHBuildJavaWrapper.GNUTLS_PC));
        return installationPrefixDir.toFile();
    }
}
