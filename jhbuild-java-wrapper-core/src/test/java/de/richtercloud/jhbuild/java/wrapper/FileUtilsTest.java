/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.compress.utils.Sets;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class FileUtilsTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(FileUtilsTest.class);
    private static final String SUBSRC = "subsrc";
    private static final String SUBFILE = "subfile";
    private static final String CERT_FILE_NAME = "NetLock_Arany_=Class_Gold=_Főtanúsítvány.pem";
    @Rule
    public TestName name = new TestName();

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void copyFolder() throws IOException {
        LOGGER.info(name.getMethodName());
        Path base = Files.createTempDirectory(FileUtilsTest.class.getSimpleName());
        LOGGER.info("base: {}", base);
        Path src = Files.createDirectory(Paths.get(base.toString(), "src"));
        Path subsrc = Files.createDirectory(Paths.get(src.toString(), SUBSRC));
        Files.createFile(Paths.get(src.toString(), "file"));
        Files.createFile(Paths.get(subsrc.toString(), SUBFILE));
        Path dest = Files.createTempDirectory(FileUtilsTest.class.getSimpleName());
        LOGGER.info("dest: {}", dest);

        FileUtils.copyFolder(src, dest);

        assertTrue("destination directory has been deleted", dest.toFile().exists());
        assertEquals(2, dest.toFile().list().length);
        assertEquals(Sets.newHashSet(SUBSRC, "file"), Sets.newHashSet(dest.toFile().list()));
        Path destSubsrc = Paths.get(dest.toString(), SUBSRC);
        assertEquals(1, destSubsrc.toFile().list().length);
        assertEquals(SUBFILE, destSubsrc.toFile().list()[0]);
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void copyFolderCanCopyEscapedHungarianCertFileName() throws IOException, URISyntaxException {
        LOGGER.info(name.getMethodName());
        Path base = Files.createTempDirectory(FileUtilsTest.class.getSimpleName());
        LOGGER.info("base: {}", base);
        Path src = Files.createDirectory(Paths.get(base.toString(), "src"));
        Path subsrc = Files.createDirectory(Paths.get(src.toString(), SUBSRC));
        Files.copy(Paths.get(FileUtilsTest.class.getResource("/cert.pem").toURI()),
                Paths.get(subsrc.toString(), "NetLock_Arany_=Class_Gold=_Főtanúsítvány.pem"));
        Path dest = Files.createTempDirectory(FileUtilsTest.class.getSimpleName());
        LOGGER.info("dest: {}", dest);

        FileUtils.copyFolder(src, dest);

        assertTrue("destination directory has been deleted", dest.toFile().exists());
        assertEquals(1, dest.toFile().list().length);
        assertEquals(SUBSRC, dest.toFile().list()[0]);
        Path destSubsrc = Paths.get(dest.toString(), SUBSRC);
        assertEquals(1, destSubsrc.toFile().list().length);
        assertEquals(CERT_FILE_NAME, destSubsrc.toFile().list()[0]);
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void copyFolderMergeDirectoryStructure() throws IOException {
        LOGGER.info(name.getMethodName());
        Path base = Files.createTempDirectory(FileUtilsTest.class.getSimpleName());
        LOGGER.info("base: {}", base);
        Path src = Files.createDirectory(Paths.get(base.toString(), "src"));
        Path subsrc = Files.createDirectory(Paths.get(src.toString(), SUBSRC));
        Files.createFile(Paths.get(subsrc.toString(), SUBFILE));
        Path dest = Files.createTempDirectory(FileUtilsTest.class.getSimpleName());
        LOGGER.info("dest: {}", dest);
        Path destSubsrc = Files.createDirectory(Paths.get(dest.toString(), SUBSRC));

        FileUtils.copyFolder(src, dest);

        assertTrue("destination directory has been deleted", dest.toFile().exists());
        assertEquals(1, dest.toFile().list().length);
        assertEquals(SUBSRC, dest.toFile().list()[0]);
        assertEquals(1, destSubsrc.toFile().list().length);
        assertEquals(SUBFILE, destSubsrc.toFile().list()[0]);
    }
}
