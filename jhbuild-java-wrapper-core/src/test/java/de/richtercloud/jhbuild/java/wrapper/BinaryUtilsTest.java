/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper;

import de.richtercloud.execution.tools.BinaryUtils;
import de.richtercloud.execution.tools.BinaryValidationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class BinaryUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BinaryUtilsTest.class);
    private static final String SOME_BINARY = "someBinary";
    /**
     * Reflect changes to the test method name automatically. There seems to be no easy way to print the test method
     * name before start although the realization would be rather trivial. Needs to be {@code public}.
     */
    @Rule
    public TestName name = new TestName();

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBinaryBinaryNull() throws BinaryValidationException {
        LOGGER.info(name.getMethodName());
        String binary = null;
        String name = "";
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBinaryBinaryEmpty() throws BinaryValidationException {
        LOGGER.info(name.getMethodName());
        String binary = "";
        String name = "";
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBinaryNameNull() throws BinaryValidationException {
        LOGGER.info(name.getMethodName());
        String binary = "validBinary";
        String name = null;
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBinaryNameEmpty() throws BinaryValidationException {
        LOGGER.info(name.getMethodName());
        String binary = "validBinary";
        String name = "";
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBinaryExistingPathNotFile() throws BinaryValidationException,
            IOException {
        LOGGER.info(name.getMethodName());
        Path existingBinaryNoFile = Files.createTempDirectory(BinaryUtilsTest.class.getSimpleName() // prefix
                );
        String binary = existingBinaryNoFile.toString();
        String name = SOME_BINARY;
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBinaryExistingPathCannotRead() throws BinaryValidationException,
            IOException {
        LOGGER.info(name.getMethodName());
        Path existingBinaryNoFile = Files.createTempFile(BinaryUtilsTest.class.getSimpleName(), // prefix
                "existingBinaryCannotRead", //suffix
                PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("r--r--r--")));
        String binary = existingBinaryNoFile.toString();
        String name = SOME_BINARY;
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testValidateBinaryExistingPath() throws BinaryValidationException,
            IOException {
        LOGGER.info(name.getMethodName());
        Path existingBinaryNoFile = Files.createTempFile(BinaryUtilsTest.class.getSimpleName(), // prefix
                "existingBinaryCannotRead", //suffix
                PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("r-xr--r--")));
        String binary = existingBinaryNoFile.toString();
        String name = SOME_BINARY;
        String path = "";
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testValidateBinaryFoundInPath() throws BinaryValidationException,
            IOException {
        LOGGER.info(name.getMethodName());
        Path binaryFoundInPath = Files.createTempFile(BinaryUtilsTest.class.getSimpleName(), // prefix
                "foundInPath", //suffix
                PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("r-xr--r--")));
        String binary = binaryFoundInPath.toString();
        String name = SOME_BINARY;
        String path = String.join(File.pathSeparator,
                Paths.get("/", "bin").toString(),
                binaryFoundInPath.getParent().toString());
        BinaryUtils.validateBinary(binary, name, path);
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testValidateBinaryNotFoundInPath() throws BinaryValidationException,
            IOException {
        LOGGER.info(name.getMethodName());
        Path binaryNotFoundInPath = Files.createTempFile(BinaryUtilsTest.class.getSimpleName(), // prefix
                "notFoundInPath", //suffix
                PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("r-xr--r--")));
        String binary = binaryNotFoundInPath.toString();
        String name = SOME_BINARY;
        String path = Paths.get("/", "bin").toString();
        BinaryUtils.validateBinary(binary, name, path);
    }
}
