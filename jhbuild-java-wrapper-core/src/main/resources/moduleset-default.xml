<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<moduleset>
    <!-- an ever-growing collection of dependencies of modules which is used as
      fallback if the user didn't specify a moduleset -->
    <repository type="tarball" name="postgresql"
                href="https://ftp.postgresql.org/pub/source/"/>
    <repository type="tarball" name="ftp.gnu.org"
                href="https://ftp.gnu.org/gnu/"/>
    <repository type="tarball" name="zlib.net"
                href="https://zlib.net/fossils/"/>
    <repository type="tarball" name="mysql"
                href="https://downloads.mysql.com/archives/get/p/23/file/"/>
    <repository type="tarball" name="cmake.org"
                href="https://cmake.org/files/"/>
    <repository type="tarball" name="github.com-tarball"
                href="https://github.com/"/>
    <repository type="git" name="github.com-git"
                href="https://github.com/"/>
    <repository type="tarball" name="bintray"
                href="https://dl.bintray.com/boostorg/release/"/>
    <repository type="tarball" name="sourceforge.net"
                href="https://netix.dl.sourceforge.net/project/"/>
    <repository name="python" type="tarball"
                href="https://www.python.org/ftp/python/"/>
    <repository name="fossies.org" type="tarball"
                href="https://fossies.org/linux/misc/"/>

    <autotools id="postgresql-10.5" autogen-sh="configure">
        <branch repo="postgresql" module="v10.5/postgresql-10.5.tar.bz2" version="10.5"/>
        <dependencies>
            <dep package="readline-6.3"/>
                <!-- dependency on readline-7.0 causes `gawk: symbol lookup error: /home/richter/prefix/lib/libreadline.so.7: undefined symbol: UP` -->
            <dep package="zlib-1.2.11"/>
            <dep package="flex-2.6.3"/>
                <!-- avoid `    *** The installed version of Flex, /usr/bin/flex, is too old to use with PostgreSQL.
                    *** Flex version 2.5.31 or later is required, but this is flex 2.6.4.` -->
                <!-- flex 2.6.4 fails to install because of `make[2]: *** [Makefile:1834: stage1scan.c] Speicherzugriffsfehler (Speicherauszug erstellt)` -->
        </dependencies>
    </autotools>
    <autotools id="postgresql-9.6.17" autogen-sh="configure">
        <branch repo="postgresql" module="v9.6.17/postgresql-9.6.17.tar.bz2" version="9.6.17"/>
        <dependencies>
            <dep package="readline-7.0"/>
            <dep package="zlib-1.2.11"/>
        </dependencies>
    </autotools>
    <autotools id="postgresql-9.5.21" autogen-sh="configure">
        <branch repo="postgresql" module="v9.5.21/postgresql-9.5.21.tar.bz2" version="9.5.21"/>
        <dependencies>
            <dep package="readline-7.0"/>
            <dep package="zlib-1.2.11"/>
        </dependencies>
    </autotools>

    <cmake id="mysql-5.7.28" use-ninja="no">
        <branch repo="mysql" module="mysql-5.7.28.tar.gz" version="5.7.28"/>
        <dependencies>
            <dep package="cmake-3.12.3"/>
            <dep package="ninja-1.8.2"/>
            <dep package="boost-1.59.0"/>
                <!-- need 1.59.0 in order to avoid `Boost minor version found is 68 we need 59` -->
            <dep package="ncurses-6.1"/>
            <dep package="bison-3.1"/>
        </dependencies>
    </cmake>
    <cmake id="mysql-5.6.46" use-ninja="no">
        <branch repo="mysql" module="mysql-5.6.46.tar.gz" version="5.6.46"/>
        <dependencies>
            <dep package="cmake-3.12.3"/>
            <dep package="ninja-1.8.2"/>
            <dep package="boost-1.59.0"/>
                <!-- need 1.59.0 in order to avoid `Boost minor version found is 68 we need 59` -->
            <dep package="ncurses-6.1"/>
            <dep package="bison-3.1"/>
        </dependencies>
    </cmake>

    <autotools id="readline-6.3" autogen-sh="configure">
        <branch repo="ftp.gnu.org" module="readline/readline-6.3.tar.gz" version="6.3"/>
        <dependencies>
            <dep package="ncurses-6.1"/>
                <!-- avoids `libreadline.so: undefined reference to `tputs'` -->
        </dependencies>
    </autotools>
    <autotools id="readline-7.0" autogen-sh="configure">
        <branch repo="ftp.gnu.org" module="readline/readline-7.0.tar.gz" version="7.0"/>
        <dependencies>
            <dep package="ncurses-6.1"/>
                <!-- avoids `libreadline.so: undefined reference to `tputs'` -->
        </dependencies>
    </autotools>
    <autotools id="ncurses-6.1" autogen-sh="configure">
        <branch repo="ftp.gnu.org" module="ncurses/ncurses-6.1.tar.gz" version="6.1"/>
    </autotools>
    <autotools id="zlib-1.2.11" autogen-template="%(srcdir)s/%(autogen-sh)s --prefix %(prefix)s">
        <branch repo="zlib.net" module="zlib-1.2.11.tar.gz" version="1.2.11"/>
    </autotools>
    <autotools id="cmake-3.12.3" autogen-template="%(srcdir)s/configure --prefix=%(prefix)s">
        <branch repo="cmake.org" module="v3.12/cmake-3.12.3.tar.gz" version="3.12.3"/>
    </autotools>
    <autotools id="boost-1.59.0" autogen-template="bash -c 'cd %(srcdir)s &amp;&amp; ./bootstrap.sh --prefix=%(prefix)s &amp;&amp; ./b2 --prefix=%(prefix)s -j8 -s BZIP2_INCLUDE=%(prefix)s/include -s BZIP2_LIBPATH=%(prefix)s/lib &amp;&amp; ./b2 --prefix=%(prefix)s install -s BZIP2_INCLUDE=%(prefix)s/include -s BZIP2_LIBPATH=%(prefix)s/lib'" makeargs="--version" makeinstallargs="--version" supports-non-srcdir-builds="no" skip-install="true">
            <!-- `./b2 &#x2D;&#x2D;prefix` is for target `install` only according to `./b2 &#x2D;&#x2D;help`
            - need to `&#x2D;&#x2D;exec-prefix` as well according to `./bootstrap.sh &#x2D;&#x2D;help`
            - `skip-install="true"` is necessary in order to be able to use `autotools` for build and installation with arbitrary command (chain), asked https://askubuntu.com/questions/1092011/how-to-build-a-jhbuild-moduleset-with-an-arbitrary-command-chain for input -->
        <branch repo="sourceforge.net" module="boost/boost/1.59.0/boost_1_59_0.tar.gz" version="1.59.0"/>
        <dependencies>
            <dep package="python-2.7.15"/>
                <!-- avoid `./boost/python/detail/wrap_python.hpp:50:23: fatal error: pyconfig.h: No such file or directory` -->
            <dep package="bzip2-1.0.6"/>
                <!-- - avoid `libs/iostreams/src/bzip2.cpp:20:56: fatal error: bzlib.h: No such file or directory`
                - needs the dynamically linked version which is controlled by choosing a different Makefile, see bzip-1.0.6 for details -->
        </dependencies>
    </autotools>
    <autotools id="bison-3.1">
        <branch repo="ftp.gnu.org" module="bison/bison-3.1.tar.gz" version="3.1"/>
        <dependencies>
            <dep package="m4-1.4.17"/>
                <!-- avoid `configure: error: no acceptable m4 could be found in $PATH. GNU M4 1.4.6 or later is required; 1.4.16 or newer is recommended. GNU M4 1.4.15 uses a buggy replacement strstr on some systems. Glibc 2.9 - 2.12 and GNU M4 1.4.11 - 1.4.15 have another strstr bug.` -->
        </dependencies>
    </autotools>
    <autotools id="flex-2.6.3" autogen-sh="configure" supports-non-srcdir-builds="no">
        <!-- `supports-non-srcdir-builds="no"` avoids `autoreconf: 'configure.ac' or 'configure.in' is required` -->
        <branch repo="github.com-tarball" module="westes/flex/releases/download/v2.6.3/flex-2.6.3.tar.gz" version="2.6.3"/>
        <dependencies>
            <dep package="m4-1.4.17"/>
                <!-- avoid `configure: error: could not find m4 that supports -P` -->
                <!-- 1.4.18 fails due to `make[3]: *** No rule to make target '/tmp/JHBuildJavaWrapperIT-download5228726490730660704/m4-1.4.18/lib/gl_avltree_oset.c', needed by 'gl_avltree_oset.o'.  Stop.` -->
            <dep package="bison-3.1"/>
                <!-- avoid `../build-aux/ylwrap: line 176: yacc: command not found` -->
        </dependencies>
    </autotools>
    <if condition-unset="nom4">
        <autotools id="m4-1.4.17">
            <branch repo="ftp.gnu.org" module="m4/m4-1.4.17.tar.gz" version="1.4.17"/>
        </autotools>
    </if>
    <autotools id="python-2.7.15">
        <branch repo="python" module="2.7.15/Python-2.7.15.tgz" version="2.7.15"/>
    </autotools>
    <cmake id="bzip2-1.0.6" use-ninja="no" cmakeargs="-DBUILD_SHARED_LIBS=true">
        <branch repo="github.com-git" module="osrf/bzip2_cmake.git" version="master"/>
        <dependencies>
            <dep package="cmake-3.12.3"/>
        </dependencies>
    </cmake>
</moduleset>
