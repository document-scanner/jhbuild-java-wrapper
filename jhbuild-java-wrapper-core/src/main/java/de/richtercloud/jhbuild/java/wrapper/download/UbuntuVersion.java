/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper.download;

import java.util.Objects;

/**
 * A data class encapsulating information about the Ubuntu version this software is executed on.
 */
public class UbuntuVersion {
    /**
     * The major version of the release, e.g. 14 in 14.04 or 14.04.3.
     */
    private final int major;
    /**
     * The minor version of the release. For Ubuntu usually 04 or 10 because they're release in April and October
     * regularly.
     */
    private final int minor;
    /**
     * The optional hotfix version of the release, e.g. 2 in 12.04.2. {@code -1} indicates that the version doesn't
     * have a hotfix version.
     */
    private final int hotfix;

    public UbuntuVersion(int major,
            int minor,
            int hotfix) {
        this.major = major;
        this.minor = minor;
        this.hotfix = hotfix;
    }

    public UbuntuVersion(int major,
            int minor) {
        this(major,
                minor,
                -1);
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getHotfix() {
        return hotfix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UbuntuVersion that = (UbuntuVersion) o;
        return major == that.major &&
                minor == that.minor &&
                hotfix == that.hotfix;
    }

    @Override
    public int hashCode() {
        return Objects.hash(major, minor, hotfix);
    }
}
