/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper.download;

/**
 * Indicates mismatch between expected and actual MD5 checksum of a file.
 */
public class MD5SumCheckUnequalsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MD5SumCheckUnequalsException(String expectedMD5Sum,
                                        String actualMD5Sum) {
        super(String.format("The MD5 checksum comparison failed (expected: %s, actual: %s)",
                expectedMD5Sum,
                actualMD5Sum));
    }

    public MD5SumCheckUnequalsException(String expectedMD5Sum,
            String actualMD5Sum,
            int numberOfRetries) {
        super(String.format("The MD5 checksum comparison failed (expected: %s, actual: %s) (retried %d times)",
                expectedMD5Sum,
                actualMD5Sum,
                numberOfRetries));
    }
}
